import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from "react-router-dom";
import Speech from './components/Speech';
import './App.css';

export default function App() {

  const pages = [
    {
      pageLink: '/',
      view: Home,
      displayName: 'Home Page'
    },
    {
      pageLink: '/about',
      view: About,
      displayName: 'About Page'
    },
    {
      pageLink: '/users',
      view: Users,
      displayName: 'Users Page'
    }
  ];

  return (
    <Router>
      <div>
        <nav>
          {pages.map((page, i) => {
            return (
              <li>
                <Link to={page.pageLink}>{page.displayName}</Link>
              </li>
            );
          })}
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}

        {pages.map((page, i) => {
          return (
            <Route
              exact
              path={page.pageLink}
              component={page.view}
              key={i}
            />
          );
        })}
        {/* <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/users">
            <Users />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch> */}
      </div>
    </Router>
  );
}

function Home() {
  return <h2><Speech /></h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}
